#!/usr/bin/env bash
#
# gitdeploy
#
# Kleines Helferskript um die Homepage auf einem Server zu bauen und im webroot zu deployen.
#
#
# Die Aufgaben sind recht simpel:
#
# * erstelle einen klon des Projekts, falls nicht schon geschehen.
# * wechsel in einen entsprechenden branch, tag oder commit
# * baue die Seite mit jbake
# * kopiere die Daten in das webroot
# * räume auf
# * sende eine Nachricht in den IRC channel bei Erfolg oder Fehlschlag
#
source ~/.bashrc

GIT_REPO="https://gitlab.com/devlug/devlug-web.git"
WEBROOT="$HOME/html/"
PROJECT_HOME="$HOME/git/"
LOGFILE="$HOME/log/deployment.log"
OUTPUT="$HOME/tmp/output/"
IRC_NICK="devlug-deploy"
IRC_SERVER="chat.freenode.net"
IRC_CHANNEL="#devlug"
DEBUG=0

function log() {
    local LEVEL=$1
    local MESSAGE=${@:2}
    echo "`date` - [$LEVEL] - $MESSAGE"
}

function debug() {
    if [ $DEBUG -eq 1 ]; then
        log "DEBUG" "$@"
    fi
}

function info() {
    log "INFO" "$@"
}

function warn() {
    log "WARN" "$@"
}

function error() {
    log "ERROR" "$@"
}

function send_IRC_msg() {

    local ANSIBLE
    ANSIBLE=$(which ansible)

    if [ $? -ne 0 ]; then
        warn "$ANSIBLE - unable to send irc message. install ansible."
    else
        if [ $DEBUG -eq 1 ]; then
            OPTIONS="-vvv"
        fi
        $ANSIBLE localhost $OPTIONS -m irc -a "nick=$IRC_NICK server=$IRC_SERVER channel=$IRC_CHANNEL msg=\"$@\""
    fi
}

function send_IRC_error() {

    send_IRC_msg "FAILED :( $@. Schau im log $LOGFILE nach was da los ist."
}

function create_project_home() {

    if [ ! -d $PROJECT_HOME ]; then
        info "$PROJECT_HOME does not exist. create directory."
        mkdir -p $PROJECT_HOME
    fi
}

function git_get_latest_tag() {
    cd $PROJECT_HOME
    echo $(git describe --abbrev=0 --tags)
}

function clone_repository() {

    create_project_home

    if [ ! -d "$PROJECT_HOME/.git" ]; then
        info "$PROJECT_HOME is not a git repository. clone project."
        git clone $GIT_REPO $PROJECT_HOME
    else
        info "$PROJECT_HOME already exists."
    fi
}

function handle_error() {
    if [ $? -ne 0 ]; then
        send_IRC_error "git $1 ist fehlgeschlagen"
        exit 1
    fi
}

function fetch_and_checkout() {
   
    info "fetch latest changes and checkout"
    cd $PROJECT_HOME
    git checkout master
    git reset --hard HEAD
    git clean -d -f
    git fetch --all
    handle_error "fetch"
    git pull origin master
    handle_error "pull origin master"
    
    if [ -z $1 ]; then
        TARGET=$(git_get_latest_tag)
    else
        TARGET=$1
    fi
    
    git checkout -f $TARGET
    handle_error "checkout -f $TARGET"
}

function bake_site() {

    local JBAKE
    JBAKE=$(which jbake)

    if [ $? -ne 0 ]; then
        error "$JBAKE - please install jbake. e.g.: sdk install jbake"
        send_IRC_error "Jbake konnte nicht gefunden werden."
        exit 1
    fi

    info "start baking website"
    cd $PROJECT_HOME
    $JBAKE -b www/jbake $OUTPUT
}

function deploy_site() {

    if [ $? -eq 0 ]; then
        info "deploy baked site to webroot"
        rsync -r -v --exclude=\.htaccess --delete-after $OUTPUT $WEBROOT
        send_IRC_msg "SUCCESS \o/ Unsere Webseite wurde erfolgreich gebacken und deployed -> https://devlug.de"
    else
        warn "interrupt deployment. something went wrong with the last execution."
        send_IRC_error "Beim sync ist was schief gegangen."
    fi
}

function clean_up() {
    info "delete output directory"
    rm -r $OUTPUT
}


#### MAIN ####

if [ -n "$2" ]; then
    info "using $2 as IRC channel to announce message."
    IRC_CHANNEL="$2"
fi

if [ ! -d "$HOME/log/" ]; then
    mkdir -p "$HOME/log"
fi

{
clone_repository
fetch_and_checkout $1
bake_site
deploy_site
clean_up
info "output logged to $LOGFILE"
} | tee $LOGFILE
