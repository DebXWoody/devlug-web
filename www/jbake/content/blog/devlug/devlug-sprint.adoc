= devLUG-Sprint
DebXWoody
2019-06-07
:jbake-type: post
:jbake-status: published
:jbake-tags: devlug
:jbake-updated: Sa 8. Jun 11:02:22 CEST 2019
:idprefix:
:sectanchors:
:sectlinks:
:toc:
[abstract]
Hier findest du Informationen zu devLUG-Sprints.

== Was sind devLUG-Sprints?
Ein Sprint ist ein zeitlich definierter Zeitraum, in dem wir uns um bestimmte
Aufgaben kümmern und Themen erarbeiten. Ein Sprint dauert genau einen Monat und
beginnt immer zum 1. eines Monats. Die Sprints werden mit YYYY-MM bezeichnet.
Also war der Sprint im September 2018 der Sprint 2018-09.

Die Aufgaben und Themen sind frei wählbar. Es kann die Vorbereitung eines
Vortrags sein, eine Dokumentation erstellen oder die Korrektur eines Artikels
sein. Das Themea sollte jedoch direkt oder indirekt etwas mit Linux oder der
devLUG zu tun haben. Für die Themen erzeugen wir einen Issue im devLUG Projekt
auf gitlab.


