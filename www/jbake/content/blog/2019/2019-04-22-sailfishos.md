title=SailfishOS mit dem Sony Xperia X
author=mase
date=2019-04-22
type=post
tags=sailfishos,mobile
status=published
~~~~~~


# Worum geht es?
Hier möchte ich meine Erfahrungen mit SailfishOS auf dem Sony Xperia X schildern. Dies beinhaltete auch die Synchronisation von Kontakten, Kalendern und Dateien, wozu ein Nextcloud Server verwendet wird.

# Einleitung
Ich habe mich im Jahr 2018 zum Kauf eines Sony Xperia X entschieden, da es die Möglichkeit bietet, neben Custom AOSP ROMs, auch SailfishOS zu installieren. Für dieses Gerät erschien im selben Jahr ein offizielles SailfishOS heraus, welches unter dem Namen Sailfish X herausgegeben wurde. Ich habe mich dabei für die Kaufversion entschieden, da diese eine Android Runtime namens Alien Dalvik enthält, die es ermöglichen soll, Android Anwendungen auszuführen. Diese Version kostet knapp 50€. Das Xperia X selber ist, Stand Februar 2019, neu für knapp 200€ zu haben.

Zunächst habe ich das Gerät mit Omnirom 7 betrieben, dann später mit Omnirom 8, jeweils ohne die Google Services. Als App Quelle kam F-Droid zum Einsatz. Seit Omnirom 8 habe ich wegen Kernel 4.x mit erheblichen Bluetooth Audio Problemen zu kämpfen, was den Betrieb von Bluetooth Headsets oder dem Betrieb an der KFZ-Anlage erschwert, bzw. nahezu unmöglich macht. Die Audioverbindung ist sehr instabil. Die Entwicklung von Omnirom 8 wurde nicht mehr weitergeführt, aber es gibt erste Testimages von Omnirom 9, was Android 9 entspricht. Das Bluetooth Problem wurde dort endlich behoben. Aber es sind viele nützliche Features verschwunden, oder sie werden nachgeliefert. Ein Grund mehr, sich SailfishOS mal wieder anzuschauen.

# Installation
Die Installation wird auf der Website von Jolla gut beschrieben. Zunächst muß man das SailfishOS Image und das OEM Image von Sony herunterladen. Das SailfishOS Image muß entpackt werden, danach plaziert man das OEM Image im selben Ordner. Das Xperia X muß dazu auf die neueste offizielle (Stock) Version von Android gebracht werden. Dazu kann das Flashtool verwendet werden. Dieses findet man auf <https://sonymobileflasher.com>

Nachdem das Gerät mit der neuesten Sony Firmware geflasht wurde, muß man es ein mal starten. Dann wartet man auf die Aufforderung, das Gerät auszuschalten.

Danach muß der Bootloader entsperrt werden. Wie das funktioniert, ist auf der Website von Sony beschrieben. Deshalb will ich jetzt darauf nicht näher eingehen. Nun muß das Gerät in den Bootloader-Modus gebracht werden. Dazu schaltet man das Gerät aus, hält die Lauter-Taste fest, und verbindet das Gerät per USB mit dem PC. Die LED muß nun blau leuchten.

Als root fährt man nun flash.sh im Ordner des SailfishOS Images aus. Nun kann man den Flashvorgang beobachten. Wenn dieser abgeschlossen ist, startet man das Gerät neu. SailfishOS sollte nun starten.

# Erstinbetriebnahme
Bei der Erstinbetriebnahme werden einige Daten abgefragt, wie WLAN Passwort, Jolla-Konto usw. Danach bekommt man eine kurze Einführung in die Bedienung, die man durch kurzes Tippen in die 4 Bildschirmecken im Uhrzeigersinn überspringen kann.

In den Einstellungen sollte man die Installation von Fremdsoftware erlauben, da wir hauptsächlich auf Apps von OpenRepos.net zugreifen.

Es empfielt sich außerdem, den Entwicklermodus zu aktivieren. Das ermöglicht unter anderem, auf das Gerät per ssh zuzugreifen, Zugriff auf das gesamte Dateisystem zu erhalten, und einen Terminal auf dem Gerät zu erhalten.

Auf den Aptoide Store hab ich bewußt verzichtet, da ich diesen nicht als besonders vertrauenswürdig ansehe. Das bleibt jedem selbst überlassen.

Die Micro-SD Karte läßt sich in den Einstellungen mit ext4 formatieren. Den Vorteil gegenüber FAT32 brauch ich wohl nicht zu erwähnen.

# Weitere Appstores
So viel ich weiß, gibt es einige Einschränkungen seitens Jolla für die Apps im offiziellen Jolla Store. Dazu gehört z. B. daß Apps nicht im Hintergrund laufen dürfen, oder die MPRIS-Unterstützung, damit die Headset Buttons auch mit anderen Mediaplayern funktionieren. Warum Jolla das verbietet, ist mir schleierhaft. Außerdem scheint im Jolla Store nur wenig Entwicklung stattzufinden.

Die neueste Software erhält man von <https://openrepos.net>. Man lädt von dort die App `Storeman` herunter und installiert diese. Um Apps zu installieren, muß man zunächst die Quelle des jeweiligen Entwicklers hinzufügen, bevor man die Option zum Installieren der App bekommt.

Zudem installieren wir den F-Droid Store von <https://fdroid.org>

Wer unbedingt Apps von Google Play braucht, kann zusätzlich den `Yalp Store` installieren, welchen man im F-Droid Store findet.


# SSH
In den Entwicklereinstellungen kann ein Passwort für den Benutzer `nemo` festgelegt werden. Dies dient als SSH Passwort. Natürlich kann man auch einen Zugang über einen SSH-Key einrichten. Dies funktioniert genauso, wie bei einem Linux-PC. Darauf will ich nicht weiter eingehen.

Mit dem Befehl `devel-su` kann man in der Konsole root Rechte erhalten. Das Passwort entspricht dem für SSH.

# AVRCP
Um nicht nur den eingebauten Mediaplayer mit den Buttons eines Bluetooth-Headsets steuern zu können, ist eine weitere Maßnahme erforderlich. Man legt im Ordner `/home/nemo/.config/systemd/user` die Datei `mpris-proxy.service` mit folgendem Inhalt an:

[Unit]
Description=mpris-proxy
Requires=lipstick.service
After=lipstick.service

[Service]
Type=simple
ExecStart=/usr/bin/mpris-proxy

[Install]
WantedBy=post-user-session.target

Die nun angelegte Systemd Servicedatei muß mit

`systemctl --user enable mpris-proxy.service`

und

`systemctl --user start mpris-proxy.service`

aktiviert und gestartet werden.

# Automatisierung
Es besteht die Möglichkeit, unter bestimmten Bedingungen bestimmte Aktionen ausführen zu lassen. Dazu gibt es im  Jolla Store die App `Situations`. Die App ist weitgehend selbsterklärend. Man kann z. B. festlegen, daß das Gerät auf lautlos schaltet, sobald man seine Heim-Funkzelle verläßt. Hier greift schon eine Restriktion von Jolla. Die App darf nicht automatisch beim Booten gestartet werden. Aber bei OpenRepos.net gibt es eine Autostart App für Situations, die man zusätzlich installieren kann.

# Bluetooth Audio
Bei Bluetooth Audio begannen die ersten Probleme. Es gab Stottern in der Wiedergabe alle paar Sekunden. Das liegt daren, daß die WLAN-Suche aktiv ist, wenn man nicht mit einem WLAN verbunden ist. Abhilfe schafft das Deaktivieren von WLAN. Ich habe mit der Situations App eingestellt, daß WLAN bei Bluetooth-Verbindungen deaktiviert wird, wenn ich nicht zu Hause, also nicht in meiner Heim-Funkzelle bin.

Nun gibt es aber immer noch Stottern, exakt alle 3 Minuten. Ich konnte noch nicht herausfinden, was die Ursache hierfür ist. Weder der `dbus-monitor`, noch `journalctl` gaben Aufschluß.

# Patches
Man kann von OpenRepos die App `Patchmanager` installieren. Entweder installiert man Patches über OpenRepos, oder im Webkatalog in Patchmanager selber. Ich war allerdings sparsam mit Patches, da ich mir nicht sicher bin, was die Kompatibilität bei OS Updates angeht. Mit Patches kann man z. B. die automatische Großschreibung bei der Tastatur deaktivieren, oder Tracker konfigurieren, womit die Medienbibliothek indiziert wird.

# Sailfish-Utilities
Im Jolla Store sollte man sich die `Sailfish-Utilities` installieren. Diese geben weitere Möglichkeiten in den Einstellungen, wie z. B. die Medienbibliothek zu leeren.

# Rsync
In OpenRepos.net gibt es die App `Simple & quick file backup`. Damit kann man Ordner im Filesystem über Rsync und SSH auf einen Server syncen. Damit habe ich meinen Homeordner gesichert. Man kann, wie bei Linux gewohnt, beliebig Symlinks erstellen, so daß jeder beliebige Ordner mit gesynct wird.

# Nextcloud
SailfishOS verfügt bereits über einen Caldav / Carddav Client, die Synchronisation von Kalender und Kontakten mit Nextcloud funktioniert somit problemlos. Für Dateien habe ich die App `Sailsync` verwendet, die recht ordentlich funktioniert, jedoch noch keinen Autosync mitbringt.

# Password-Safe
Mit der App `Ownkeepass` steht eine Keepass Implementierung zur Verfügung. Das Datenbankfile habe ich mit `Sailsync` (siehe Abschnitt Nextcloud) synchronisiert.

# Webbrowser
Leider ist der eingebaute Webbrowser sehr alt und unterstützt viele neue Funktionen nicht. Eine kleine Verbesserung ist der Webbrowser `Webcat`. Es empfielt sich aber, `Fennec` von F-Droid zu nutzen. Es bleibt zu hoffen, daß sich hier noch was tut.

# Messenger
SailfishOS verfügt über einen eingebauten XMPP Client, der aber kaum Features besitzt. Verschlüsselung fehlt hier ganz. Auch hier sollte man auf `Conversations` von F-Droid setzen. Allerdings funktionieren bei Android Messengern die Echtzeit-Benachrichtigungen nicht besonders gut. Auch `Riot` für Matrix ist da betroffen. Es kann also passieren, daß Nachrichten erst sehr verspätet ankommen. Es bleibt zu beachten, daß in den Einstellungen für die Apps, ganz unten im SailfishOS Einstellungsmenü, die Hintergrundaktivitäten von Android Apps erlaubt werden müssen. Natürlich nur bei denen, wo es Sinn macht. Es bleibt zu hoffen, daß irgendwann native Messenger verfügbar sind.

# Fazit
Das Bedienkonzept von SailfishOS gefällt mir persönlich sehr gut, an vielen Stellen um einiges besser, als das bei Android der Fall ist. Während bei Android gefühlt mit jeder neuen Version Funktionen wegfallen, wird SailfishOS ständig besser, wenn auch oft an den falschen Stellen, z. B. findet viel Kostmetik statt. Es mangelt jedenfalls massiv an sinnvollen nativen Apps, man muß also oft auf Android Apps zurückgreifen. Als Linux Fan fühlt man sich jedenfalls sehr wohl. Man hat direkt das Gefühl, "zu Hause" zu sein. Umso bedauerlicher finde ich, daß das OS für mich noch nicht für den täglichen Gebrauch geeignet ist, was aber nur an der mangelhaften Funktion von Bluetooth und dem Fehlen von nativen Messengern liegt. Trotzdem bleibt das OS einen Blick wert. Ich werde es auf jeden Fall weiter beobachten.
