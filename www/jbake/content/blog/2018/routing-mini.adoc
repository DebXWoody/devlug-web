= Routing - Beispiele
Plocki <plocki@devlug.de>
{localdate} 
:jbake-type: post
:jbake-status: draft
:jbake-tags: ip
:idprefix:
:imagesdir: /img
:toc:
[abstract]
Routing anhand einiger Beispiele

== Vorwort 
Alle folgenden Beispiele setzen vorraus, dass IP-forwarding auf den Routern aktiviert ist. Dazu muss in der /etc/sysctl.conf folgene Zeile vorhanden sein:
[source,text]
net.ipv4.ip_forward = 1

Anschließend aktivieren mit:
[source,text]
sysctl -p 

== Kurzreferenz

Das Routingsystem geht alle Routing-Tabellen von 0 aufsteigend (=Priorität) durch, bis ein Routingkriterium passt oder die gesamte Regelliste durchlaufen ist. 

Alle Routingregeln ansehen:
[source,text]
ip rule list

Spezifische Routingtabelle ansehen:
[source,text]
ip route list table 200

Wird keine Tabelle angegeben, bezieht sich die Ausgabe immer auf Tabelle main.


== Beispiel 1: Zwei Netzwerke LAN1 und LAN2 über Router (R0) verbinden
image::/img/article/routing/2nets.png[Pic1]
=== Möglichkeit 1: Über Destination-Routing
Auf Sytemen in LAN1:
[source,text]
ip route add 192.168.9.0/24 via 192.168.8.1 dev eth0

Auf Systemen in LAN2:
[source,text]
ip route add 192.168.0.0/24 via 192.168.9.1 dev eth0


=== Möglichkeit 2: Über NAT/Masquerading
Auf R0 zwei Masquerading Regeln anlegen:
[source,text]
iptables -t nat -A POSTROUTING -o eth0 -s 192.168.9.0/24 -j MASQUERADE
iptables -t nat -A POSTROUTING -o eth1 -s 192.168.8.0/24 -j MASQUERADE

== Beispiel 2: Gastnetz in anonymes VPN leiten
=== Ist-Zustand
image::/img/article/routing/2netsvpn.png[Pic2]

* Ein Linuxrechner (R1) mit 2 Interfaces im LAN mit aktiviertem VPN (hier: openVPN)

    eth0 IP: 192.168.8.2
    Netmask: 255.255.255.0 
    Defaultgateway (R2): 192.168.8.1
    
	eth1 IP: 192.168.9.1 (=Gastnetz)
	Netmask: 255.255.255.0
	
    tun0 IP: 10.8.8.1 (dynamisch, VPN-Netz oder "Transit-Netz")
	
* Beispiel Routingtabelle main
[source,text]
$ ip route
0.0.0.0/1 via 10.8.8.1 dev tun0                                     # VPN Defaultroute #1
default via 192.168.8.1 dev eth0  metric 202                        # original Defaultroute
10.8.8.0/24 dev tun0  proto kernel  scope link  src 10.8.8.7        # Tunnel Netzwerk
128.0.0.0/1 via 10.8.8.1 dev tun0                                   # VPN Defaultroute #2
192.96.203.70 via 192.168.8.1 dev eth0                              # Hostroute zu VPN Endpunkt
192.168.8.0/24 dev eth0  proto kernel  scope link  src 192.168.8.2  # LAN1 Netzroute (Homenetz)
192.168.9.0/24 dev eth1  proto kernel  scope link  src 192.168.9.1  # LAN2 Netzroute (Gastnetz)

Hinweis: VPN-Defaultrouten 0.0.0.0/1 und 128.0.0.0/1 überdecken original Defaultroute!

=== Soll-Zustand

Rechner aus dem Gastnetz sollen über VPN geroutet werden, Homenetz weiterhin über Defaultgateway.

* VPN Defaultrouten entferenen
* eigene Routen/Regeln erstellen
* NAT an tun0 für Gastnetz 


=== Lösung: Source-Routing
Die von openVPN automatisch gesetzten Defaultrouten müssen deaktiviert werden und durch eigene Routen+Regeln ersetzt werden, z.B. über up.- und down-Skripte. Dazu in der openvpn.conf folgende Zeilen einfügen:
[source,text]
route-nopull		# verhindert Pulling der Routen von Gegenstelle
script-security 2	# erlaubt up/down Skripte
up vpn-up.sh		# definiert up-Skript
down vpn-down.sh	# definiert down-Skript

Die eigenen Regeln und Routen lassen sich nun in den up.- und down-Skripte setzten.

Achtung: Die Gateway-IP für das Gastnetz (192.168.9.1) muss von den Regeln ausgenommen werden, daher definieren wir uns den IP-Bereich 192.168.9.2 bis 192.168.9.255 selber:
[source,text]
$ cat /etc/openvpn/vpn-up.sh
#!/bin/sh
ip rule add from 192.168.9.2/31 prio 30000 table 200
ip rule add from 192.168.9.4/30 prio 30000 table 200
ip rule add from 192.168.9.8/29 prio 30000 table 200
ip rule add from 192.168.9.16/28 prio 30000 table 200
ip rule add from 192.168.9.32/27 prio 30000 table 200
ip rule add from 192.168.9.64/26 prio 30000 table 200
ip rule add from 192.168.9.128/25 prio 30000 table 200
ip route add default table 200 via $4 dev tun0
iptables -t nat -A POSTROUTING -o tun0 -s 192.168.9.0/24 -j MASQUERADE

[source,text]
$ cat /etc/openvpn/vpn-down.sh
#!/bin/sh
ip rule del from 192.168.9.2/31 prio 30000 table 200
ip rule del from 192.168.9.4/30 prio 30000 table 200
ip rule del from 192.168.9.8/29 prio 30000 table 200
ip rule del from 192.168.9.16/28 prio 30000 table 200
ip rule del from 192.168.9.32/27 prio 30000 table 200
ip rule del from 192.168.9.64/26 prio 30000 table 200
ip rule del from 192.168.9.128/25 prio 30000 table 200
ip route del default table 200 via $4 dev tun0
iptables -t nat -D POSTROUTING -o tun0 -s 192.168.9.0/24 -j MASQUERADE

Erläuterungen: 
"ip rule from ..." definiert Regeln für Source-Adressen, ihrer Priorität und verweist auf eine Routing-Tabelle #200. Einträge in diese Tabelle erfolgen dann mit "ip route add table 200 ...". In diesem Beispiel wird nur eine Defaultroute für den Tunnel gesetzt. 

Ggf. können hier weitere Regeln folgen, z.B. um LAN von Guest-LAN aus erreichbar zu machen, und/oder umgekehrt. Siehe Beispiel 1.

Darüber hinaus ist es denkbar, auch Systeme aus LAN1 durch umbiegen ihrer Defaultroute ebenfalls über das VPN zu leiten. Damit lässt sich z.B. vom Desktop aus via Skript der VPN Zugang an und abzuschalten. 


== Beispiel 3: Mehrere Defaultgateways gleichzeitig nutzen

=== Ist-Zustand
* Ein Linuxrechner mit 2 Interfaces im LAN 

* Zwei (oder mehr) Defaultgateways

=== Beide Defaultgateways gleichzeigtig/gemeinsam nutzen 
Roundrobin(RR) by MARK, TOS, Load balancer, Source/Destination-Port, VRRP, und usw...

Todo... 
 
 
