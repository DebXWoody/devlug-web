= Terminal Anwendungen 
DebXWoody
2018-04-07
:jbake-type: post
:jbake-status: published
:jbake-tags: terminal 
:jbake-regal: Anwendungen
:jbake-autoren: DebXWoody 
:jbake-updated: Sa 4. Aug 08:28:48 CEST 2018
:idprefix:
:toc:
[abstract]
Wofür baucht man X? Es gibt viele sehr gute Terminal-Anwendungen. Wir stellen
hier mal ein paar davon vor.

.Terminal Apps
[#/img-terminal-apps]
[caption="Figure 1: "]
image::/img/article/Terminal-Apps.png[Terminal Apps,width="80%",align="center"]

== Vim - Editor
Vim steht für Vi IMproved und ist ein verbesserter vi-Editor. Der Editor hat
eigentlich fast alles was man so braucht.

.VIM
[#/img-vim]
[caption="Figure 2: "]
image::/img/article/vim.png[VIM,width="80%",align="center"]

== Neomutt - E-Mail
Ein sehr flexibles E-Mail-Programm. Wer Konsolen-Anwendungen bevorzugt, der wird
neomutt lieben. ;-)

.neomutt
[#/img-neomutt]
[caption="Figure 3: "]
image::/img/article/neomutt.png[neomutt,width="80%",align="center"]

== abook - Adressbuch
Ein ncurses Adressbuch. Abook hat eine query-Funktion um abook in neomutt zu
integrieren.

.abook
[#/img-abook]
[caption="Figure 4: "]
image::/img/article/abook.png[abook,width="80%",align="center"]

http://abook.sourceforge.net/

	aptitude install abook

== Taskwarrior - Aufgaben
Aufgaben und ToDo-Liste.

https://taskwarrior.org

	aptitude install timewarrior

== Timewarrior - Zeiterfassung
Zeiterfassung

.timew
[#/img-timew]
[caption="Figure 5: "]
image::/img/article/timew800.png[abook,width="80%",align="center"]

	aptitude install timewarrior

Dokumentation: https://taskwarrior.org/docs/timewarrior/

== WeeChat - IRC-Client

